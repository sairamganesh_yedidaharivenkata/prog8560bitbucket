How to Build and Install this project
1. Download all files
2. Open in editor like Visual studio code
3. Run on live server

LICENSE: MIT LICENSE
The MIT license is a great choice because it allows you to share your code under a copyleft license without forcing others to expose their proprietary code, it's business friendly and open source friendly while still allowing for monetization.